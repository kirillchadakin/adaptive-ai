package com.smartcat.trader.ai;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.Signal;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Queue;
import java.util.*;
import java.util.stream.Collectors;

public class SignalsTable {
    private final SignalTableConfiguration config;

    public SignalsTable(SignalTableConfiguration config) {
        this.config = config;
    }

    public void saveImg(Queue<Signal> signals) {
        BufferedImage image = new BufferedImage(
                config.getWidth(), config.getHeight(), BufferedImage.TYPE_INT_RGB);
        drawTable((Graphics2D) image.getGraphics(), signals);
        try {
            ImageIO.write(image, "png", new File("snapshot.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawTable(Graphics2D g, Queue<Signal> signals) {
        g.setBackground(Color.WHITE);
        g.fillRect(0, 0, config.getWidth(), config.getHeight());
        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(1));
        int tableWidth = config.getWidth() - config.getxOffset() * 2;
        int tableHeight = config.getHeight() - config.getyOffset() * 2;
        g.drawRect(config.getxOffset(), config.getyOffset(), tableWidth, tableHeight);

        if (signals.isEmpty()) {
            return;
        }
        Instant maxCreated = signals.stream().map(Signal::getCreated).max(Instant::compareTo).get();
        Instant minInstant = signals.stream().map(Signal::getCreated).min(Instant::compareTo).get();
        Instant maxInstant = maxCreated.plus(config.getTtl());
        long delta = Duration.between(minInstant, maxInstant).toMillis();
        float coef = (float) tableHeight / delta;
        Map<Reference, List<Signal>> neuronsToSignals = signals.stream().collect(
                Collectors.groupingBy(signal -> signal.getTargetInput().getSource(),
                        () -> new TreeMap<>(Comparator.comparingLong(Reference::getId)), Collectors.toList()));
        int colWidth = tableWidth / neuronsToSignals.size();
        int traceWidth = colWidth - config.getTraceOffset() * 2;
        int traceHeight = 3;//(int) (config.getTtl().toMillis() * coef);
        int x = config.getxOffset();
        for (Map.Entry<Reference, List<Signal>> entry : neuronsToSignals.entrySet()) {
            int y = config.getyOffset();
            if (entry.getValue().stream().findFirst().get() instanceof DataSignal<?>) {
                g.setColor(Color.BLACK);
            } else {
                g.setColor(Color.RED);
            }
            g.drawString(String.valueOf(entry.getKey().getId()), x, y - 10);
            for (Signal signal : entry.getValue()) {
                int traceX = x + config.getTraceOffset();
                int traceY = (int) (y + Duration.between(minInstant, signal.getCreated()).toMillis() * coef);
                g.fillRect(traceX, traceY, traceWidth, traceHeight);
            }
            x += colWidth;
        }
    }

//    private static void draw(List<Signal> signals) {
//        JFrame jFrame = new JFrame();
//        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        jFrame.add(new TablePanel(signals));
//        jFrame.pack();
//        jFrame.setVisible(true);
//    }

}
