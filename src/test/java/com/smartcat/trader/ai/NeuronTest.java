package com.smartcat.trader.ai;

import java.time.Clock;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import com.smartcat.trader.ai.neurons.*;
import com.smartcat.trader.ai.signals.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.smartcat.trader.ai.LinkFactoryImpl.Consumer;
import com.smartcat.trader.ai.LinkFactoryImpl.Producer;
import com.smartcat.trader.ai.neurons.threshold.calculator.LinearNoiseThresholdCalculator;

/**
 * Checks that {@link Neuron} implementations are working as expected.
 */
public class NeuronTest {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Predicate<DataSignal<Number>> SIGNAL_PREDICATE =
            dataSignal -> dataSignal.getValue().doubleValue() > 0;
    public static final Duration NEURON_SENDING_DELAY = Duration.ofMillis(200);
    public static final Duration SIGNAL_TTL = Duration.ofMillis(400);
    private final SignalsFactory signalsFactory =
            new SignalsFactoryImpl(Clock.systemUTC(), SIGNAL_TTL, Duration.ZERO);
    private NeuronsFactory neuronsFactory;
    private ReferencesFactory referencesFactory;
    private ExecutorService threadPool;
    private LinkFactory linkFactory;

    /**
     * Initializes all resources required for tests.
     */
    @Before
    public void before() {
        threadPool = Executors.newCachedThreadPool(
                new ThreadFactoryBuilder().setNameFormat("Neural Network Thread #%d")
                        .build());
        referencesFactory = new ReferencesFactory();
        linkFactory = new LinkFactoryImpl(Duration.ZERO, threadPool);
        neuronsFactory = new NeuronsFactoryImpl(signalsFactory, linkFactory, NEURON_SENDING_DELAY,
                referencesFactory, new LinearNoiseThresholdCalculator(1, 0.7),
                new StatisticCheckerFactoryImpl());
    }

    /**
     * Releases all resources occupied by tests.
     */
    @After
    public void after() {
        threadPool.shutdownNow();
    }

    /**
     * Checks that {@link Neuron} can train and send signals.
     * Neuron has sent at least one data and one disabling signals after it has fully trained.
     *
     * @throws InterruptedException in case testing thread was interrupted.
     */
    @Test
    public void checkNeuronTrainingAndSendingSignals() throws InterruptedException {
        final Neuron neuron1 = neuronsFactory.createNeuron(SIGNAL_PREDICATE, 2, 2);
        final Consumer consumer = neuron1.getConsumer(SignalType.Disabling);
        final BlockingQueue<Signal> sensorSignals = new LinkedBlockingQueue<>();
        final Producer producer = linkFactory.producer(sensorSignals::take).build();
        final Queue<Signal> neuronSignalsHistory = new ConcurrentLinkedQueue<>();
        final Consumer neuronSignalsHistoryDataConsumer = linkFactory.consumer(DataSignal.class, neuronSignalsHistory::add).build();
        final Consumer neuronSignalsHistoryDisablingConsumer = linkFactory.consumer(DisablingSignal.class, neuronSignalsHistory::add).build();
        neuron1.connect(neuronSignalsHistoryDataConsumer, Duration.ZERO, SignalType.Data);
        neuron1.connect(neuronSignalsHistoryDisablingConsumer, Duration.ZERO, SignalType.Disabling);
        neuron1.connect(producer, Duration.ZERO, SignalType.Data);
        final Reference sensorId = producer.getRef();
        IntStream.range(1, 6).forEach(item -> sensorSignals
                .add(signalsFactory.createDataSignal(sensorId, item)));
        threadPool.awaitTermination(2, TimeUnit.SECONDS);
        boolean hasDataSignal = neuronSignalsHistory.stream().anyMatch(signal -> signal instanceof DataSignal);
        boolean hasDisablingSignal = neuronSignalsHistory.stream().anyMatch(signal -> signal instanceof DisablingSignal);
        Assert.assertTrue(hasDataSignal);
        Assert.assertTrue(hasDisablingSignal);
    }

    @Test
    public void checkSignalKeeping() throws InterruptedException {
        final Neuron neuron1 = neuronsFactory.createNeuron(SIGNAL_PREDICATE, 2, 2);
        final Consumer consumer = neuron1.getConsumer(SignalType.Disabling);
        final BlockingQueue<Signal> sensorSignals = new LinkedBlockingQueue<>();
        final Producer producer = linkFactory.producer(sensorSignals::take).build();
        final Queue<Signal> neuronSignalsHistory = new ConcurrentLinkedQueue<>();
        final Consumer neuronSignalsHistoryDataConsumer = linkFactory.consumer(DataSignal.class, neuronSignalsHistory::add).build();
        neuron1.connect(neuronSignalsHistoryDataConsumer, Duration.ZERO, SignalType.Data);
        neuron1.connect(producer, Duration.ZERO, SignalType.Data);
        final Reference sensorId = producer.getRef();
        IntStream.range(1, 6).forEach(item -> sensorSignals
                .add(signalsFactory.createDataSignal(sensorId, item)));
        threadPool.awaitTermination(4, TimeUnit.SECONDS);
        Assert.assertThat(neuronSignalsHistory.size(), CoreMatchers.is(8));
    }

    @Test
    public void checkNeuronFeedback() throws InterruptedException {
        final Neuron neuron1 = neuronsFactory.createNeuron(SIGNAL_PREDICATE, 2, 2);
        final Consumer disablingConsumer = neuron1.getConsumer(SignalType.Disabling);
        final BlockingQueue<Signal> sensorSignals = new LinkedBlockingQueue<>();
        final Producer producer = linkFactory.producer(sensorSignals::take).build();
        final Queue<Signal> neuronSignalsHistory = new ConcurrentLinkedQueue<>();
        final Consumer neuronSignalsHistoryDataConsumer = linkFactory.consumer(DataSignal.class, neuronSignalsHistory::add).build();
        final Consumer neuronSignalsHistoryDisablingConsumer = linkFactory.consumer(DisablingSignal.class, neuronSignalsHistory::add).build();
        producer.connect(neuronSignalsHistoryDataConsumer, Duration.ZERO);
        neuron1.connect(neuronSignalsHistoryDataConsumer, Duration.ZERO, SignalType.Data);
        neuron1.connect(neuronSignalsHistoryDisablingConsumer, Duration.ZERO, SignalType.Disabling);
        neuron1.connect(disablingConsumer, Duration.ZERO, SignalType.Disabling);
        neuron1.connect(producer, Duration.ZERO, SignalType.Data);
        final Reference sensorId = producer.getRef();
        IntStream.range(1, 6).forEach(item -> {
            sensorSignals
                    .add(signalsFactory.createDataSignal(sensorId, item));
        });
        threadPool.awaitTermination(3, TimeUnit.SECONDS);
        long disablingSignalsCount = neuronSignalsHistory.stream().filter(signal -> signal instanceof DisablingSignal).count();
        long dataSignalsCount = neuronSignalsHistory.stream().filter(signal -> signal instanceof DataSignal).count();
        draw(neuronSignalsHistory);
        Assert.assertThat(disablingSignalsCount, CoreMatchers.is(1L));
        Assert.assertThat(dataSignalsCount, CoreMatchers.is(3L));
    }

    @Test
    public void simpleNetworkTest() throws InterruptedException {
        final Queue<Signal> signalsHistory = new ConcurrentLinkedQueue<>();
        final Consumer signalsHistoryDataConsumer = linkFactory.consumer(DataSignal.class, signalsHistory::add).build();
        final Consumer signalsHistoryDisablingConsumer = linkFactory.consumer(DisablingSignal.class, signalsHistory::add).build();
        final Sensor[] sensors = createSensors(3);
        final Neuron[] inputLayer = createLayer(3, 3, 3, signalsHistoryDataConsumer, signalsHistoryDisablingConsumer);
        final Neuron[] hiddenLayer = createLayer(3, 3, 3, signalsHistoryDataConsumer, signalsHistoryDisablingConsumer);
        final Neuron[] outNeurons = createLayer(1, 3, 3, signalsHistoryDataConsumer, signalsHistoryDisablingConsumer);
        final Neuron outputNeuron = outNeurons[0];
        LOGGER.debug("Sensors: {}", Arrays.toString(sensors));
        LOGGER.debug("Input layer");
        logNeurons(inputLayer);
        LOGGER.debug("Hidden layer");
        logNeurons(hiddenLayer);
        LOGGER.debug("Output neuron");
        logNeurons(outNeurons);
        for (int i = 0; i < 3; i++) {
            sensors[i].getProducer().connect(signalsHistoryDataConsumer, Duration.ZERO);
            inputLayer[i].connect(sensors[i].getProducer(), Duration.ZERO, SignalType.Data);
            inputLayer[i].connect(hiddenLayer[i].getConsumer(SignalType.Data), Duration.ZERO, SignalType.Data);
            hiddenLayer[i].connect(inputLayer[i].getConsumer(SignalType.Disabling), Duration.ZERO, SignalType.Disabling);
            hiddenLayer[i].connect(outputNeuron.getConsumer(SignalType.Data), Duration.ZERO, SignalType.Data);
            outputNeuron.connect(hiddenLayer[i].getConsumer(SignalType.Disabling), Duration.ZERO, SignalType.Disabling);
        }
        int cycleAmount = 15;
        for (int i = 1; i <= cycleAmount; i++) {
            if (i >= 2 && i <= 10) {
                Arrays.stream(sensors).forEach(Sensor::createAndSendSignal);
            }
            Thread.sleep(NEURON_SENDING_DELAY.toMillis());
        }
        Thread.sleep(NEURON_SENDING_DELAY.toMillis());
        Queue<Signal> dataSignalHistory = signalsHistory.stream().filter(signal -> signal instanceof DataSignal<?>)
                .collect(Collectors.toCollection(ConcurrentLinkedQueue::new));
        draw(signalsHistory);
    }

    private void logNeurons(Neuron[] inputLayer) {
        for (Neuron neuron : inputLayer) {
            LOGGER.debug("Consumers: {}", neuron.getConsumers().stream().map(Consumer::getRef).collect(Collectors.toList()));
            LOGGER.debug("Producers: {}", neuron.getProducers().stream().map(Producer::getRef).collect(Collectors.toList()));
        }
    }

    private void draw(Queue<Signal> signals) {
        SignalTableConfiguration config = new SignalTableConfiguration();
        config.setSendDelay(Duration.ZERO);
        config.setSendDelta(NEURON_SENDING_DELAY);
        config.setTtl(SIGNAL_TTL);
        SignalsTable signalsTable = new SignalsTable(config);
        signalsTable.saveImg(signals);
    }

    private Sensor[] createSensors(int amount) {
        Sensor[] sensors = new Sensor[amount];
        for (int i = 0; i < amount; i++) {
            sensors[i] = new Sensor();
        }
        return sensors;
    }

    private Neuron[] createLayer(int neuronAmount, int lLimit, int gLimit, Consumer historyDataConsumer, Consumer historyDisablingConsumer) {
        Neuron[] layer = new Neuron[neuronAmount];
        for (int i = 0; i < neuronAmount; i++) {
            layer[i] = neuronsFactory.createNeuron(SIGNAL_PREDICATE, lLimit, gLimit);
            layer[i].connect(historyDataConsumer, Duration.ZERO, SignalType.Data);
            layer[i].connect(historyDisablingConsumer, Duration.ZERO, SignalType.Disabling);
        }
        return layer;
    }

    private class Sensor {
        private final Reference id;
        private final Producer producer;
        private final BlockingQueue<Signal> signals;
        private final AtomicLong value = new AtomicLong();

        public Sensor() {
            signals = new LinkedBlockingQueue<>();
            producer = linkFactory.producer(signals::take).build();
            id = producer.getRef();
        }

        public void createAndSendSignal() {
            Signal signal = signalsFactory.createDataSignal(id, value.incrementAndGet());
            signals.add(signal);
        }

        public Producer getProducer() {
            return producer;
        }

        public BlockingQueue<Signal> getSignals() {
            return signals;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Sensor{");
            sb.append("id=").append(id);
            sb.append('}');
            return sb.toString();
        }
    }
}
