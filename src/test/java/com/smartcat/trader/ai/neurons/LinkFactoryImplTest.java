package com.smartcat.trader.ai.neurons;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.smartcat.trader.ai.LinkFactoryImpl;
import com.smartcat.trader.ai.ReferencesFactory;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.Signal;
import com.smartcat.trader.ai.signals.SignalsFactory;
import com.smartcat.trader.ai.signals.SignalsFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Clock;
import java.time.Duration;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LinkFactoryImplTest {
    private ExecutorService threadPool;
    private LinkFactoryImpl linkFactory;
    private ReferencesFactory referencesFactory;
    private SignalsFactory signalsFactory;
    private Clock clock;

    @Before
    public void init() {
        threadPool = Executors.newCachedThreadPool(
                new ThreadFactoryBuilder().setNameFormat("Neural Network Thread #%d")
                        .build());
        linkFactory = new LinkFactoryImpl(Duration.ZERO, threadPool);
        referencesFactory = new ReferencesFactory();
        clock = Mockito.mock(Clock.class);
        signalsFactory = new SignalsFactoryImpl(clock, Duration.ofSeconds(2), Duration.ZERO);
    }

    @Test
    public void interaction() {
        Reference producerReference = referencesFactory.create(LinkFactoryImplTest.class);
        Queue<Signal> outSignals = new ConcurrentLinkedQueue<>();
        BlockingDeque<Signal> inSignals = new LinkedBlockingDeque<>();
        LinkFactoryImpl.Consumer consumer = linkFactory.consumer(DataSignal.class, outSignals::add).build();
        linkFactory.producer(inSignals::take).build().connect(consumer, Duration.ZERO);
        IntStream.range(0, 4).forEach(i -> {
            inSignals.add(signalsFactory.createDataSignal(producerReference, i));
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Assert.assertEquals(4, outSignals.size());
    }
}
