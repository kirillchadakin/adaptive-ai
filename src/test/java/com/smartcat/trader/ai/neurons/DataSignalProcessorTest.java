/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.collect.ImmutableSet;

import com.smartcat.trader.ai.signals.*;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import com.smartcat.trader.ai.LinkFactoryImpl.Producer;
import com.smartcat.trader.ai.ReferencesFactory;
import com.smartcat.trader.ai.neurons.inputs.NeuronInputImpl;
import com.smartcat.trader.ai.neurons.inputs.UniqueSourceInputs;
import com.smartcat.trader.ai.neurons.threshold.calculator.LinearNoiseThresholdCalculator;

/**
 * {@link DataSignalProcessorTest} checks that {@link DataSignalProcessor} implementation is working
 * as expected.
 */
public class DataSignalProcessorTest {

    private static final int DURATION_MS = 30;
    private static final Duration TTL = Duration.ofMillis(DURATION_MS);
    private static final Duration DELAY = Duration.ZERO;

    private final Predicate<DataSignal<Double>> signalPredicate = Mockito.spy(new TestPredicate());
    private SignalsFactory signalsFactory;
    private ReferencesFactory referencesFactory;
    private Reference sourceNeuronRef;
    private NoiseThresholdCalculator thresholdCalculator;
    private BlockingQueue<Signal> disablingInSignals;
    private BlockingQueue<Signal> dataOutSignals;
    private BlockingQueue<Signal> disablingOutSignals;
    private StatisticCheckerFactory statisticCheckerFactory;
    private Clock clock;
    private Reference signalSource;
    private Reference relatedNeuronRef;

    /**
     * Initializes all resources required by tests.
     */
    @Before
    public void before() {
        clock = Mockito.mock(Clock.class);
        signalsFactory = new SignalsFactoryImpl(clock, TTL, DELAY);
        referencesFactory = new ReferencesFactory();
        sourceNeuronRef = referencesFactory.create(NeuronI.class);
        thresholdCalculator = Mockito.spy(new LinearNoiseThresholdCalculator(1D, 0.7));
        disablingInSignals = new LinkedBlockingQueue<>();
        dataOutSignals = new LinkedBlockingQueue<>();
        disablingOutSignals = new LinkedBlockingQueue<>();
        statisticCheckerFactory = new StatisticCheckerFactoryImpl();
        signalSource = referencesFactory.create(Producer.class);
        relatedNeuronRef = referencesFactory.create(NeuronI.class);
    }

    private <V, S extends DataSignal<V>> DataSignalProcessor<V, S> createSignalProcessor(int gLimit,
                                                                                         int lLimit, Predicate<S> dataSignalPredicate) {
        SignalKeeper signalKeeper = new SignalKeeper(dataOutSignals, disablingInSignals);
        // TODO: 21.04.2022 change producer reference
        return new DataSignalProcessor<>(sourceNeuronRef, relatedNeuronRef, relatedNeuronRef, signalsFactory, statisticCheckerFactory,
                thresholdCalculator, dataSignalPredicate, dataOutSignals, disablingOutSignals, lLimit, gLimit,
                new UniqueSourceInputs<>(relatedNeuronRef), signalKeeper);

    }

    /**
     * Checks that two valid signals will cause {@link DataSignalProcessor} to prepare data out
     * signal. Additionally checks that invalid signal will be ignored and will not cause creation
     * of a new data out signal.
     */
    @Test
    public void checkCreationOfOneOutSignal() {
        final DataSignalProcessor<Double, DataSignal<Double>> processor =
                createSignalProcessor(1, 1, signalPredicate);
        processor.register(new NeuronInputImpl(signalSource, DELAY));
        final DataSignal<Double> validSignal =
                Mockito.spy(signalsFactory.createDataSignal(signalSource, 2D));
        Mockito.doReturn(true).when(validSignal).isRelevant(Mockito.any());
        Mockito.doReturn(false).when(validSignal).isOutdated(Mockito.any());
        // Registration of a new source, so signal will be omitted
        processor.process(validSignal);
        processor.process(validSignal);
        Mockito.doReturn(true).when(validSignal).isOutdated(Mockito.any());
        processor.process(validSignal);
        final DataSignal<Double> invalidSignal = Mockito.spy(signalsFactory.createDataSignal(signalSource, 0D));
        Mockito.doReturn(true).when(invalidSignal).isRelevant(Mockito.any());
        Mockito.doReturn(false).when(invalidSignal).isOutdated(Mockito.any());
        processor.process(invalidSignal);
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(1));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(0));
    }

    /**
     * Checks that invalid {@link Signal}, i.e. which will not pass {@link Predicate} will not come
     * to  {@link NoiseThresholdCalculator} and as a result will not affect on the current noise
     * threshold.
     */
    @Test
    public void checksThatInvalidSignalWillNotAffectOnThreshold() {
        final SignalProcessor<DataSignal<Double>> processor =
                createSignalProcessor(1, 1, signalPredicate);
        final DataSignal<Double> invalidSignal = signalsFactory.createDataSignal(signalSource, 0D);
        processor.process(invalidSignal);
        processor.process(invalidSignal);
        Mockito.verify(thresholdCalculator, Mockito.never())
                .calculate(Mockito.anyDouble(), Mockito.anyLong());
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(0));
    }

    /**
     * Checks that invalid {@link Signal}, i.e. which will not pass {@link Predicate} will not come
     * to  {@link NoiseThresholdCalculator} and as a result will not affect on the current noise
     * threshold.
     */
    @Test
    public void checkPassingGLimitBySignals() {
        final int gLimit = 2;
        final int lLimit = 2;
        final DataSignalProcessor<Double, DataSignal<Double>> processor =
                createSignalProcessor(gLimit, lLimit, signalPredicate);
        processor.register(new NeuronInputImpl(signalSource, DELAY));
        IntStream.rangeClosed(0, gLimit + lLimit + 1).forEach(i -> {
            final DataSignal<Double> signal =
                    Mockito.spy(signalsFactory.createDataSignal(signalSource, i + 1D));
            Mockito.doReturn(true).when(signal).isRelevant(Mockito.any());
            Mockito.doReturn(false).when(signal).isOutdated(Mockito.any());
            processor.process(signal);
        });
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(4));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(2));
    }

    /**
     * Checks that invalid {@link Signal}, i.e. which will not pass {@link Predicate} will not come
     * to  {@link NoiseThresholdCalculator} and as a result will not affect on the current noise
     * threshold.
     */
    @Test
    public void checksOutdatedEventWillNotBeActive() {
        final SignalProcessor<DataSignal<Double>> processor =
                createSignalProcessor(1, 1, signalPredicate);
        final DataSignal<Double> signal = signalsFactory.createDataSignal(signalSource, 2D);
        processor.process(signal);
        Mockito.verify(signalPredicate, Mockito.never()).test(Mockito.any());
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(0));
    }

    /**
     * Checks several use-cases:
     * <ol>
     *     <li> Adding {@link Signal} from a new source will not cause processing
     *     of the {@link Signal}.
     *     <li> Reducing of the activation threshold causes activation with less amount
     *     of active inputs.
     *     <li> Checks that disabling signal will not allow for related {@link Neuron}
     *     instance to send data output {@link Signal}.
     * </ol>
     */
    @Test
    public void checkMultipleSourcesSignals() {
        final DataSignalProcessor<Double, DataSignal<Double>> processor =
                createSignalProcessor(1, 1, signalPredicate);
        final int amountOfIncomingConnections = 10;
        final List<Reference> dataProviders =
                IntStream.range(0, amountOfIncomingConnections).mapToObj(id -> {
                    final Reference reference = referencesFactory.create(Producer.class);
                    processor.register(new NeuronInputImpl(reference, DELAY));
                    return reference;
                }).collect(Collectors.toList());
        Collection<DataSignal<Double>> previousSignals = Collections.emptySet();
        final Collection<Integer> stagesToDecreaseActiveNeurons = ImmutableSet.of(5, 7, 8);
        int activeNeurons = amountOfIncomingConnections;
        final Reference disableSignalSource = referencesFactory.create(Producer.class);
        for (int i = 0; i < amountOfIncomingConnections; i++) {
            if (stagesToDecreaseActiveNeurons.contains(i)) {
                activeNeurons--;
            }
            if (i == 5) {
                disablingInSignals.add(signalsFactory.createDisablingSignal(disableSignalSource));
            }
            final Collection<DataSignal<Double>> signals = new ArrayList<>(dataProviders.size());
            for (int j = 0; j < dataProviders.size(); j++) {
                final DataSignal<Double> signal = Mockito.spy(signalsFactory
                        .createDataSignal(dataProviders.get(j), j + 1D));
                Mockito.doReturn(j >= activeNeurons).when(signal).isOutdated(Mockito.any());
                Mockito.doReturn(true).when(signal).isRelevant(Mockito.any());
                signals.add(signal);
            }
            previousSignals.forEach(signal -> Mockito.doReturn(true).when(signal)
                    .isOutdated(Mockito.any()));
            signals.forEach(processor::process);
            previousSignals = signals;
        }
        Mockito.verify(thresholdCalculator, Mockito.times(23))
                .calculate(Mockito.anyDouble(), Mockito.anyLong());
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(21));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(21));
    }

    /**
     * Checks that in case there are more than one active event for the same source, than latest
     * (recently received) signal will take precedence.
     */
    @Test
    public void checkMultipleSourcesMultipleActiveEvents() {
        final DataSignalProcessor<Double, DataSignal<Double>> processor =
                createSignalProcessor(1, 1, signalPredicate);
        final int amountOfIncomingConnections = 10;
        final Collection<Reference> dataProviders =
                IntStream.range(0, amountOfIncomingConnections).mapToObj(id -> {
                    final Reference reference = referencesFactory.create(Producer.class);
                    processor.register(new NeuronInputImpl(reference, Duration.ZERO));
                    return reference;
                }).collect(Collectors.toList());
        final AtomicLong predicateFailureCounter = new AtomicLong();
        Mockito.doAnswer((Answer<Boolean>) invocation -> {
            final Object result = invocation.callRealMethod();
            if (Boolean.FALSE.equals(result)) {
                predicateFailureCounter.incrementAndGet();
            }
            return null;
        }).when(signalPredicate).test(Mockito.any());
        for (int i = 0; i < 3; i++) {
            final Collection<DataSignal<Double>> signals = new ArrayList<>(dataProviders.size());
            for (Reference source : dataProviders) {
                final long sourceId = source.getId();
                final DataSignal<Double> signal =
                        Mockito.spy(signalsFactory.createDataSignal(source, sourceId + 1D));
                Mockito.doReturn(false).when(signal).isOutdated(Mockito.any());
                Mockito.doReturn(true).when(signal).isRelevant(Mockito.any());
                if (i >= 1) {
                    Mockito.doReturn(0D).when(signal).getValue();
                }
                signals.add(signal);
            }
            signals.forEach(processor::process);
        }
        Assert.assertThat(dataOutSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingInSignals.size(), CoreMatchers.is(0));
        Assert.assertThat(disablingOutSignals.size(), CoreMatchers.is(0));
    }

    @Test
    public void checksThatInputDelayConsideredInActivationHandling() {

    }

    private void stubClockValues(Double... durationMultipliers) {
        final List<Long> millis = Arrays.stream(durationMultipliers)
                .mapToLong(i -> Math.round(i * DURATION_MS) + (Objects.equals(0D, i) ?
                        0 :
                        1)).boxed().collect(Collectors.toList());
        final Queue<Instant> instants = millis.stream().map(Instant::ofEpochMilli)
                .collect(Collectors.toCollection(LinkedList::new));
        Mockito.when(clock.instant()).thenAnswer(invocation -> instants.poll());
    }

    /**
     * {@link TestPredicate} predicate that is checking that data signal has value greater than
     * one.
     */
    private static class TestPredicate implements Predicate<DataSignal<Double>> {
        @Override
        public boolean test(DataSignal<Double> doubleDataSignal) {
            return doubleDataSignal.getValue() > 0D;
        }
    }
}
