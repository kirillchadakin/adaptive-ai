package com.smartcat.trader.ai;

import java.time.Duration;

public class SignalTableConfiguration {
    private Duration sendDelay;
    private Duration ttl;
    private Duration sendDelta;
    private int width = 800;
    private int height = 800;
    private int xOffset = 25;
    private int yOffset = 25;
    private int traceOffset = 10;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getxOffset() {
        return xOffset;
    }

    public void setxOffset(int xOffset) {
        this.xOffset = xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }

    public void setyOffset(int yOffset) {
        this.yOffset = yOffset;
    }

    public Duration getSendDelay() {
        return sendDelay;
    }

    public void setSendDelay(Duration sendDelay) {
        this.sendDelay = sendDelay;
    }

    public Duration getTtl() {
        return ttl;
    }

    public void setTtl(Duration ttl) {
        this.ttl = ttl;
    }

    public Duration getSendDelta() {
        return sendDelta;
    }

    public void setSendDelta(Duration sendDelta) {
        this.sendDelta = sendDelta;
    }

    public int getTraceOffset() {
        return traceOffset;
    }

    public void setTraceOffset(int traceOffset) {
        this.traceOffset = traceOffset;
    }
}