/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai;

import javax.annotation.Nonnull;

/**
 * {@link Builder} common interface to build instances of desired type.
 *
 * @param <T> type of the instances that builder is able to build.
 */
public interface Builder<T> {
    /**
     * Creates instance of desired type with desired parameters.
     *
     * @return instance of desired type with desired parameters.
     */
    @Nonnull
    T build();
}
