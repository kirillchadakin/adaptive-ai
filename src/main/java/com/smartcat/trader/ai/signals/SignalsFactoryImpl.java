/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.signals;

import java.time.Clock;
import java.time.Duration;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * {@link SignalsFactoryImpl} creates instances of {@link Signal} interface with some predefined
 * parameters.
 */
public class SignalsFactoryImpl implements SignalsFactory {
    private final Clock clock;
    private final Duration ttl;
    private final Duration defaultDelay;

    public SignalsFactoryImpl(@Nonnull Clock clock, @Nonnull Duration ttl,
                    @Nonnull Duration defaultDelay) {
        this.clock = clock;
        this.ttl = ttl;
        this.defaultDelay = defaultDelay;
    }

    @Override
    @Nonnull
    public <V> DataSignal<V> createDataSignal(@Nonnull Reference source, @Nonnull V value,
                    @Nonnull Duration delay) {
        return new DataSignal<>(source, clock, ttl, value, delay);
    }

    @Nonnull
    @Override
    public <V> DataSignal<V> createDataSignal(@Nonnull Reference source, @Nonnull V value) {
        return new DataSignal<>(source, clock, ttl, value, defaultDelay);
    }

    @Override
    @Nonnull
    public Signal createDisablingSignal(@Nonnull Reference source, @Nonnull Duration delay) {
        return new DisablingSignal(source, clock, ttl, delay);
    }

    @Nonnull
    @Override
    public Signal createDisablingSignal(@Nonnull Reference source) {
        return new DisablingSignal(source, clock, ttl, defaultDelay);
    }
}
