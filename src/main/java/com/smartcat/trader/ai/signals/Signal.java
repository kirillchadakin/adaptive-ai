/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.signals;

import java.time.Duration;
import java.time.Instant;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.neurons.inputs.NeuronInput;

/**
 * {@link Signal} describes common functionality of the signals in the system.
 */
public interface Signal {

    /**
     * Returns pointer to the {@link NeuronInput} where signal should arrive.
     *
     * @return pointer to {@link NeuronInput} in the target signal consumer.
     */
    @Nonnull
    NeuronInput getTargetInput();

    /**
     * Returns {@code true} in case a signal should be treated as outdated, i.e. its TTL value +
     * created value becomes lower than current timestamp, otherwise returns {@code false}.
     *
     * @param inputDelay delay from input after which {@link Signal} instance should
     *                 be treated as relevant.
     * @return whether signal should be treated as outdated
     */
    boolean isOutdated(Duration inputDelay);

    /**
     * Returns {@link Instant} moment when {@link Signal} instance was created.
     *
     * @return moment when {@link Signal} instance was created.
     */
    @Nonnull
    Instant getCreated();

    /**
     * Creates signal with all equals fields except for reference.
     *
     * @param reference new signal {@link Reference}.
     * @return new {@link Signal}.
     */
    Signal with(Reference reference);

}
