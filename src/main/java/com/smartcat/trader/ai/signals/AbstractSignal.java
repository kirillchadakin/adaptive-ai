/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.signals;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.neurons.inputs.NeuronInput;
import com.smartcat.trader.ai.neurons.inputs.NeuronInputImpl;

/**
 * {@link AbstractSignal} provides common implementation of the {@link Signal} interface.
 */
public abstract class AbstractSignal implements Signal, Comparable<AbstractSignal> {
    private final Instant created;
    private final Duration ttl;
    private final Clock clock;
    private final NeuronInput targetInput;

    protected AbstractSignal(@Nonnull Reference source, @Nonnull Clock clock, @Nonnull Duration ttl,
                    @Nonnull Duration delay) {
        this.targetInput = new NeuronInputImpl(source, delay);
        this.ttl = ttl;
        this.created = clock.instant();
        this.clock = clock;
    }

    protected Duration getTtl() {
        return ttl;
    }

    protected Clock getClock() {
        return clock;
    }

    @Nonnull
    @Override
    public NeuronInput getTargetInput() {
        return targetInput;
    }

    @Override
    @Nonnull
    public Instant getCreated() {
        return created;
    }

    public boolean isRelevant(Duration delay) {
        final Instant currentTime = clock.instant();
        final Instant delayedCreated = created.plus(delay);
        return delayedCreated.isAfter(currentTime) && !isOutdated(delay);
    }

    @Override
    public boolean isOutdated(Duration inputDelay) {
        final Instant currentTime = clock.instant();
        return created.plus(inputDelay).plus(ttl).isBefore(currentTime);
    }

    @Override
    public String toString() {
        return String.format("%s [targetInput=%s, created=%s, ttl=%s]", getClass().getSimpleName(),
                        this.targetInput, this.created, this.ttl);
    }

    @Override
    public int compareTo(AbstractSignal o) {
        final int sourceOrdering = targetInput.compareTo(o.targetInput);
        if (sourceOrdering != 0) {
            return sourceOrdering;
        }
        return getCreated().compareTo(o.getCreated());
    }
}
