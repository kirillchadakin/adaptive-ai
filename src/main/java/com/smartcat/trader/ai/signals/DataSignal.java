package com.smartcat.trader.ai.signals;

import java.time.Clock;
import java.time.Duration;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * {@link DataSignal} signal that carries data.
 *
 * @param <V> type of the data associated with a signal.
 */
public class DataSignal<V> extends AbstractSignal {
    private final V value;

    /**
     * Creates {@link DataSignal} instance.
     *
     * @param source reference to the source that has sent this signal.
     * @param clock provides information about current time.
     * @param ttl time to live
     * @param value data
     * @param delay delay before making the signal active
     */
    public DataSignal(@Nonnull Reference source, @Nonnull Clock clock, @Nonnull Duration ttl,
                    @Nonnull V value, Duration delay) {
        super(source, clock, ttl, delay);
        this.value = value;
    }

    /**
     * Returns data associated with the {@link DataSignal} instance.
     *
     * @return data associated
     */
    @Nonnull
    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%s [targetInput=%s, value=%s, created=%s]", getClass().getSimpleName(),
                        this.getTargetInput(), this.value, this.getCreated());
    }

    /**
     * Creates signal with all equals fields except for reference.
     *
     * @param reference new signal {@link Reference}.
     * @return new {@link Signal}.
     */
    @Override
    public Signal with(Reference reference) {
        return new DataSignal<>(reference, getClock(), getTtl(), value, getTargetInput().getDelay());
    }
}
