/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.signals;

import java.time.Clock;
import java.time.Duration;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * DisablingSignal.
 */
public class DisablingSignal extends AbstractSignal {

    public DisablingSignal(Reference source, Clock clock, Duration ttl, Duration delay) {
        super(source, clock, ttl, delay);
    }

    /**
     * Creates signal with all equals fields except for reference.
     *
     * @param reference new signal {@link Reference}.
     * @return new {@link Signal}.
     */
    @Override
    public Signal with(Reference reference) {
        return new DisablingSignal(reference, getClock(), getTtl(), getTargetInput().getDelay());
    }
}
