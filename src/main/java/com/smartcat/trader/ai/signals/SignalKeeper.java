package com.smartcat.trader.ai.signals;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicMarkableReference;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * Keeps signals by cashing last one stored in data out signals collection. Cashed signal is taken
 * until the disabling signal gets. Until disabling signal is not taken signal keeper gives cashed
 * signal if data out signal collection is empty otherwise it retrieves, cashes and gives signal
 * from collection.
 */
public class SignalKeeper {
    private static final Logger LOGGER = LogManager.getLogger();
    private final BlockingQueue<Signal> dataOutSignals;
    private final BlockingQueue<Signal> disablingInSignals;
    private final AtomicMarkableReference<Signal> cashedSignal =
                    new AtomicMarkableReference<>(null, false);
    // TODO: 20.04.2022 need some better way to init this
    private Reference producer;

    /**
     * Creates {@link SignalKeeper}.
     *  @param dataOutSignals produced signals.
     * @param disablingInSignals received disabling signals.
     */
    public SignalKeeper(BlockingQueue<Signal> dataOutSignals,
                        BlockingQueue<Signal> disablingInSignals) {
        this.disablingInSignals = disablingInSignals;
        this.dataOutSignals = dataOutSignals;
    }

    /**
     * If data out signals collection receives {@link Signal} from collection, cashes and return it.
     * If collection is empty and there is any signal in cash then creates new signal from the
     * cashed one with another {@link Reference} and returns it, otherwise waits for signal to occur
     * in collection.
     *
     * @return first {@link Signal} in data out collection or created from cashed one.
     * @throws InterruptedException when waiting for {@link Signal} is interrupted.
     */
    public Signal takeDataSignal() throws InterruptedException {
        final Signal outSignal = dataOutSignals.poll();
        if (outSignal != null) {
            return outSignal;
        }
        if (cashedSignal.isMarked()) {
            final Signal signal = cashedSignal.getReference();
            return signal.with(producer);
        }
        return dataOutSignals.take();
    }

    /**
     * Cashing {@link Signal}.
     *
     * @param signal will be cashed
     */
    public void enableKeeping(Signal signal) {
        cashedSignal.set(signal, true);
        LOGGER.trace("Signal keeping enabled by '{}'", signal::toString);
    }

    /**
     * Removes cashed {@link Signal} and adds one to disabling signal queue.
     *
     * @param signal disabling signal.
     */
    public void addDisablingSignal(Signal signal) {
        cashedSignal.set(null, false);
        LOGGER.trace("Signal keeping disabled by '{}'", signal::toString);
        disablingInSignals.add(signal);
    }

    public void setProducer(Reference producer) {
        this.producer = producer;
    }
}
