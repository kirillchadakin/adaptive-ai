/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.signals;

import java.time.Duration;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * SignalsFactory.
 */
public interface SignalsFactory {

    @Nonnull
    <V> DataSignal<V> createDataSignal(@Nonnull Reference source, @Nonnull V value,
                    @Nonnull Duration delay);
    @Nonnull
    <V> DataSignal<V> createDataSignal(@Nonnull Reference source, @Nonnull V value);

    @Nonnull
    Signal createDisablingSignal(@Nonnull Reference source, @Nonnull Duration delay);

    @Nonnull
    Signal createDisablingSignal(@Nonnull Reference source);
}
