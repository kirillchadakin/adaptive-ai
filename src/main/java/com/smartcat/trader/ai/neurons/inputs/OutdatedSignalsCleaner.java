/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.BiConsumer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.signals.Signal;

/**
 * Cleans outdated signals from the active signals collection.
 */
public class OutdatedSignalsCleaner
                implements BiConsumer<NeuronInput, Collection<? extends Signal>> {
    private static final String REMOVED_OUTDATED_SIGNALS_MESSAGE =
                    "Removing from '{}' source the following outdated signals: '{}'";
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void accept(NeuronInput input, Collection<? extends Signal> signalsFromInput) {
        final Iterator<? extends Signal> sfsIt = signalsFromInput.iterator();
        final Collection<Signal> outdatedSignals = new HashSet<>();
        final Duration inputDelay = input.getDelay();
        while (sfsIt.hasNext()) {
            final Signal current = sfsIt.next();
            if (current.isOutdated(inputDelay)) {
                sfsIt.remove();
                outdatedSignals.add(current);
            }
        }
        if (outdatedSignals.isEmpty()) {
            return;
        }
        final boolean isTraceEnabled = LOGGER.isTraceEnabled();
        final Level level = isTraceEnabled ? Level.TRACE : Level.DEBUG;
        LOGGER.log(level, REMOVED_OUTDATED_SIGNALS_MESSAGE, input::toString,
                        isTraceEnabled ? outdatedSignals::toString : outdatedSignals::size);
    }
}
