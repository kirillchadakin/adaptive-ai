/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.function.Predicate;

import com.smartcat.trader.ai.signals.DataSignal;

/**
 * Ne.
 */
public interface NeuronsFactory {

    <D> Neuron createNeuron(Predicate<DataSignal<D>> predicate, int lLimit, int gLimit);
}
