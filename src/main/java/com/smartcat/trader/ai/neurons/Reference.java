/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

/**
 * NeuronReference.
 */
public interface Reference {
    long getId();

}
