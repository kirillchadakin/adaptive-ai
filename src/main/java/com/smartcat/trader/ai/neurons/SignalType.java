/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

/**
 * {@link SignalType}.
 */
public enum SignalType {
    Data, Disabling
}
