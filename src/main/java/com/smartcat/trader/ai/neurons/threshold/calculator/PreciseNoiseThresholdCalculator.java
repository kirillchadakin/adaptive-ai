/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.threshold.calculator;

import com.smartcat.trader.ai.neurons.NoiseThresholdCalculator;

/**
 * PreciseNoiseThresholdCalculator.
 */
public class PreciseNoiseThresholdCalculator implements NoiseThresholdCalculator {

    @Override
    public double calculate(double current, long m) {
        return 1;
    }

    @Override
    public double getMax() {
        return 1;
    }
}
