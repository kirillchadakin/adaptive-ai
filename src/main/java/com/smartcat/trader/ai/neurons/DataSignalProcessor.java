/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.Collection;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.signals.SignalKeeper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.neurons.inputs.NeuronInput;
import com.smartcat.trader.ai.neurons.inputs.NeuronInputs;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.Signal;
import com.smartcat.trader.ai.signals.SignalsFactory;

/**
 * {@link DataSignalProcessor} process input signal.
 *
 * @param <V> type of the value which is taken from {@link DataSignal} instance.
 * @param <S> type of the {@link DataSignal} which can be processed
 */
public class DataSignalProcessor<V, S extends DataSignal<V>> implements SignalProcessor<S> {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CREATING_SIGNAL_MESSAGE = "Creating '{}' for '{}'";

    private final Predicate<S> signalChecker;
    private final NeuronInputs<V, S> inputs;
    private final NoiseThresholdCalculator noiseThresholdCalculator;
    private final Queue<Signal> dataOutSignals;
    private final Queue<Signal> disablingOutSignals;
    private final StatisticChecker<Signal> g;
    private final StatisticChecker<Signal> l;
    private final SignalKeeper signalKeeper;
    private final Reference neuron;
    private double p;

    /**
     * Creates {@link DataSignalProcessor} instance.
     *
     * @param neuron reference to neuron instance which will be served by this
     *                 processor.
     * @param signalsFactory creates new {@link Signal}s when required
     * @param statisticCheckerFactory create {@link StatisticChecker} instances.
     * @param noiseThresholdCalculator calculates noise threshold based on the
     *                 context information.
     * @param signalChecker checks whether received {@link Signal} will cause
     *                 passing of the activation rule and further processing or not.
     * @param dataOutSignals queue of the data output signals.
     * @param disablingOutSignals queue of the disabling output signals
     * @param lLimit statistic limit after which L rule will be treated as passed,
     *                 i.e. related {@link Neuron} instance will change its state to 'trained'.
     * @param gLimit statistic limit after which G rule will be treated as passed,
     *                 i.e. related {@link Neuron} instance will change its state to 'able to
     *                 predict'. {@link Neuron} will require less active inputs to pass activation
     * @param inputs inputs of the related {@link Neuron} instance.
     * @param signalKeeper enabling keeping signals.
     */
    public DataSignalProcessor(@Nonnull Reference neuron, @Nonnull Reference dataProducerReference,
                               @Nonnull Reference disableProducerReference,
                               @Nonnull SignalsFactory signalsFactory,
                    @Nonnull StatisticCheckerFactory statisticCheckerFactory,
                    @Nonnull NoiseThresholdCalculator noiseThresholdCalculator,
                    @Nonnull Predicate<S> signalChecker, @Nonnull Queue<Signal> dataOutSignals,
                    @Nonnull Queue<Signal> disablingOutSignals, long lLimit, long gLimit,
                    @Nonnull NeuronInputs<V, S> inputs, SignalKeeper signalKeeper) {
        this.signalChecker = signalChecker;
        this.dataOutSignals = dataOutSignals;
        this.disablingOutSignals = disablingOutSignals;
        this.noiseThresholdCalculator = noiseThresholdCalculator;
        this.p = noiseThresholdCalculator.getMax();
        this.neuron = neuron;
        this.signalKeeper = signalKeeper;
        this.g = statisticCheckerFactory.createStatisticChecker(gLimit, "G",
                        (value) -> signalsFactory.createDisablingSignal(disableProducerReference));
        this.l = statisticCheckerFactory.createStatisticChecker(lLimit, "L",
                        (value) -> signalsFactory.createDataSignal(dataProducerReference, value));
        this.inputs = inputs;
    }

    @Override
    public void process(@Nonnull S signal) {
        LOGGER.trace("Processing '{}' signal received by '{}'", signal::toString, neuron::toString);
        final Collection<S> latestActiveSignals = inputs.getLatestActiveSignals(signal);
        if (latestActiveSignals.isEmpty()) {
            return;
        }
        final long h = latestActiveSignals.stream().filter(signalChecker).count();
        final int m = inputs.size();
        final double pCurrent = h / (double)m;
        final boolean failedActivationRule = pCurrent < p;
        if (failedActivationRule) {
            return;
        }
        LOGGER.trace("Current activation value '{}'('{}'/'{}') passed noise threshold '{}' for '{}'",
                        pCurrent, h, m, p, neuron);
        final double newThreshold = noiseThresholdCalculator.calculate(p, m);
        if (newThreshold < p) {
            LOGGER.debug("Noise threshold for '{}' has changed to '{}'('{}'/'{}':'{}')", neuron,
                            newThreshold, h, m, pCurrent);
        }
        p = newThreshold;
        Optional.ofNullable(l.createSignal(signal, neuron))
                        .ifPresent(new CreatedSignalProcessor(signal));
    }

    /**
     * Adds {@link NeuronInput} to controlled {@link NeuronInputs}.
     *
     * @param input adding {@link NeuronInput}
     * @return true if input added successfully
     */
    public boolean register(NeuronInput input) {
        return inputs.add(input);
    }

    /**
     * {@link CreatedSignalProcessor} process recently created output {@link Signal}.
     */
    private class CreatedSignalProcessor implements Consumer<Signal> {
        private final S signal;

        private CreatedSignalProcessor(S signal) {
            this.signal = signal;
        }

        @Override
        public void accept(Signal outSignal) {
            sendSignal(outSignal, dataOutSignals);
            signalKeeper.enableKeeping(outSignal);
            final Signal disablingSignalOut = g.createSignal(signal, neuron);
            Optional.ofNullable(disablingSignalOut)
                            .ifPresent(disablingSignal -> sendSignal(disablingSignal,
                                            disablingOutSignals));
        }

        private void sendSignal(Signal signal, Queue<Signal> outSignals) {
            LOGGER.debug(CREATING_SIGNAL_MESSAGE, signal::toString, neuron::toString);
            outSignals.add(signal);
        }
    }

}
