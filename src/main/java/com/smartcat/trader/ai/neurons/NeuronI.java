/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableMap;

import com.smartcat.trader.ai.LinkFactory;
import com.smartcat.trader.ai.LinkFactoryImpl.Consumer;
import com.smartcat.trader.ai.LinkFactoryImpl.Producer;
import com.smartcat.trader.ai.neurons.inputs.NeuronInputs;
import com.smartcat.trader.ai.neurons.inputs.UniqueSourceInputs;
import com.smartcat.trader.ai.signals.*;

/**
 * {@link NeuronI} describes autonomous neuron of type I.
 *
 * @param <V> type of the data contained in {@link DataSignal}.
 */
public class NeuronI<V> implements Neuron {
    private final Map<SignalType, Consumer> incomingLinks;
    private final Map<SignalType, Producer> outgoingLinks;
    private final NeuronInputs<V, DataSignal<V>> inputs;
    private final Reference reference;

    /**
     * Creates {@link NeuronI} instance.
     *
     * @param reference identifier of the {@link Neuron}
     * @param signalChecker predicate to check whether received signal will be
     *                 processed by {@link Neuron}.
     * @param sendingDelay timeout used to avoid spoofing of the receivers neuron
     *                 output signals.
     * @param noiseThresholdCalculator calculator used to determine current noise
     *                 threshold over all inputs connected to the neuron.
     * @param lLimit limit for L rule
     * @param gLimit limit for G rule
     * @param signalsFactory factory to create data output and disabling output
     *                 signals
     * @param linkFactory factory to build links between producer/consumer inside
     *                 the neuron
     * @param statisticCheckerFactory factory to create {@link StatisticChecker}
     *                 implementations.
     */
    public NeuronI(@Nonnull Reference reference, @Nonnull Predicate<DataSignal<V>> signalChecker,
                    @Nonnull Duration sendingDelay,
                    @Nonnull NoiseThresholdCalculator noiseThresholdCalculator, long lLimit,
                    long gLimit, @Nonnull SignalsFactory signalsFactory,
                    @Nonnull LinkFactory linkFactory,
                    @Nonnull StatisticCheckerFactory statisticCheckerFactory) {
        this.reference = reference;
        final BlockingQueue<Signal> dataOutSignals = new LinkedBlockingQueue<>();
        final BlockingQueue<Signal> disablingOutSignals = new LinkedBlockingQueue<>();
        final BlockingQueue<Signal> disablingInSignals = new LinkedBlockingQueue<>();

        inputs = new UniqueSourceInputs<>(reference);
        SignalKeeper signalKeeper = new SignalKeeper(dataOutSignals, disablingInSignals);
        Producer dataProducer = linkFactory.producer(signalKeeper::takeDataSignal).setParent(reference)
                .setSendDelay(sendingDelay).build();
        Producer disableProducer = linkFactory.producer(disablingOutSignals::take).setParent(reference)
                .setSendDelay(sendingDelay).build();
        outgoingLinks = ImmutableMap.of(SignalType.Data, dataProducer, SignalType.Disabling,
                disableProducer);
        signalKeeper.setProducer(dataProducer.getRef());
        final SignalProcessor<DataSignal<V>> dataSignalProcessor =
                        new DataSignalProcessor<>(reference, dataProducer.getRef(), disableProducer.getRef(), signalsFactory,
                                        statisticCheckerFactory, noiseThresholdCalculator,
                                        signalChecker, dataOutSignals,
                                        disablingOutSignals, lLimit, gLimit, inputs, signalKeeper);
        incomingLinks = ImmutableMap.of(SignalType.Data,
                        linkFactory.consumer(DataSignal.class, dataSignalProcessor)
                                        .setParent(reference).build(), SignalType.Disabling,
                        linkFactory.consumer(DisablingSignal.class, signalKeeper::addDisablingSignal)
                                        .setParent(this).build());
    }

    @Override
    public void connect(@Nonnull Producer producer, Duration delay, @Nonnull SignalType in) {
        producer.connect(getConsumer(in), delay);
    }

    @Override
    public void connect(@Nonnull Consumer consumer, Duration delay, @Nonnull SignalType out) {
        final Producer producer = outgoingLinks.get(out);
        producer.connect(consumer, delay);
    }

    @Nonnull
    @Override
    public Consumer getConsumer(@Nonnull SignalType in) {
        return incomingLinks.get(in);
    }

    @Nonnull
    @Override
    public Reference getRef() {
        return reference;
    }

    @Override
    public String toString() {
        return String.format("%s [reference=%s]", getClass().getSimpleName(), this.reference);
    }

    @Override
    public Collection<Consumer> getConsumers() {
        return incomingLinks.values();
    }

    @Override
    public Collection<Producer> getProducers() {
        return outgoingLinks.values();
    }
}
