/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link AbstractInputs}.
 *
 * @param <S> type of the {@link DataSignal} that can be passed to the input.
 * @param <V> type of the data providing by received signals.
 */
public abstract class AbstractInputs<V, S extends DataSignal<V>> implements NeuronInputs<V, S> {
    private final Logger logger = LogManager.getLogger(getClass());
    private final Reference neuron;

    private final BiConsumer<NeuronInput, Collection<? extends Signal>> outdatedSignalsCleaner;

    protected AbstractInputs(Reference neuron,
                    BiConsumer<NeuronInput, Collection<? extends Signal>> outdatedSignalsCleaner) {
        this.neuron = neuron;
        this.outdatedSignalsCleaner = outdatedSignalsCleaner;
    }

    protected void cleanSignals(NeuronInput source, Collection<S> signals) {
        outdatedSignalsCleaner.accept(source, signals);
    }

    protected void logNewSource(NeuronInput signalSource) {
        logger.debug("New signal input '{}' registered for '{}' data processor.", signalSource,
                        neuron);
    }

    @Nonnull
    protected abstract Collection<S> collectLatestActiveSignals(@Nonnull S signal);

    @Nonnull
    @Override
    public Collection<S> getLatestActiveSignals(@Nonnull S signal) {
        final Collection<S> result = collectLatestActiveSignals(signal);
        logger.trace("'{}' has the following active signals:'{}'", neuron::toString,
                        () -> result.stream().sorted().collect(Collectors.toList()));
        return result;
    }
}
