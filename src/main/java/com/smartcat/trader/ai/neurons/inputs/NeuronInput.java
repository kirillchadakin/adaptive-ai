/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.time.Duration;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.signals.Signal;

/**
 * NeuronInput.
 */
public interface NeuronInput extends Comparable<NeuronInput> {

    /**
     * Provides a {@link Reference} to the signal source, i.e. who sent this signal.
     *
     * @return signal source.
     */
    @Nonnull
    Reference getSource();

    /**
     * Delay while which {@link Signal} instance should be treated as inactive, i.e. should not
     * participate in the data processing.
     *
     * @return delay until {@link Signal} becomes active.
     */
    @Nonnull
    Duration getDelay();
}
