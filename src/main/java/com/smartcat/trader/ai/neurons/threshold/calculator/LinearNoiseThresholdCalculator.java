/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.threshold.calculator;

import com.smartcat.trader.ai.neurons.NoiseThresholdCalculator;

/**
 * {@link LinearNoiseThresholdCalculator}.
 */
public class LinearNoiseThresholdCalculator implements NoiseThresholdCalculator {
    private final double pMax;
    private final double pMin;

    public LinearNoiseThresholdCalculator(double pMax, double pMin) {
        checkParameter(pMax, "pMax");
        checkParameter(pMin, "pMin");
        this.pMax = pMax;
        this.pMin = pMin;
    }

    private static void checkParameter(double parameter, String name) {
        if (parameter > 1 && parameter <= 0) {
            throw new IllegalArgumentException(
                            String.format("%s parameter should belong to the following interval (0; 1]",
                                            name));
        }
    }

    @Override
    public double calculate(double current, long m) {
        if (current <= pMin) {
            return pMin;
        }
        final double result = current - (pMax - pMin) / m;
        if (result <= pMin) {
            return pMin;
        }
        return result;
    }

    @Override
    public double getMax() {
        return pMax;
    }

}
