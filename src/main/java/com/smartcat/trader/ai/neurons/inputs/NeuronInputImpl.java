/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.time.Duration;
import java.util.Objects;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * {@link NeuronInputImpl} describes abstraction of the neuron input.
 */
public class NeuronInputImpl implements NeuronInput {
    private final Reference source;
    private final Duration delay;

    public NeuronInputImpl(@Nonnull Reference source, @Nonnull Duration delay) {
        this.source = source;
        this.delay = delay;
    }

    @Nonnull
    public Reference getSource() {
        return source;
    }

    @Nonnull
    @Override
    public Duration getDelay() {
        return delay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NeuronInputImpl)) {
            return false;
        }
        final NeuronInputImpl that = (NeuronInputImpl)o;
        return source.equals(that.source) && delay.equals(that.delay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, delay);
    }

    @Override
    public String toString() {
        return String.format("%s [source=%s, delay=%s]", getClass().getSimpleName(), this.source,
                        this.delay);
    }

    @Override
    public int compareTo(NeuronInput o) {
        final int sourceIdComparison = Long.compare(source.getId(), o.getSource().getId());
        if (sourceIdComparison != 0) {
            return sourceIdComparison;
        }
        return delay.compareTo(o.getDelay());
    }
}
