/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.time.Duration;
import java.util.function.Predicate;

import com.smartcat.trader.ai.LinkFactory;
import com.smartcat.trader.ai.ReferencesFactory;
import com.smartcat.trader.ai.neurons.threshold.calculator.PreciseNoiseThresholdCalculator;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.SignalsFactory;

/**
 * NeuronsFactoryImpl.
 */
public class NeuronsFactoryImpl implements NeuronsFactory {
    private static final PreciseNoiseThresholdCalculator PRECISE_NOISE_THRESHOLD_CALCULATOR =
                    new PreciseNoiseThresholdCalculator();
    private final SignalsFactory signalsFactory;
    private final LinkFactory linkFactory;
    private final Duration sendingDelay;
    private final ReferencesFactory referencesFactory;
    private final NoiseThresholdCalculator noiseThresholdCalculator;
    private final StatisticCheckerFactory statisticCheckerFactory;

    public NeuronsFactoryImpl(SignalsFactory signalsFactory, Duration sendingDelay,
                    LinkFactory linkFactory, StatisticCheckerFactory statisticCheckerFactory,
                    ReferencesFactory referencesFactory) {
        this(signalsFactory, linkFactory, sendingDelay, referencesFactory,
                        PRECISE_NOISE_THRESHOLD_CALCULATOR, statisticCheckerFactory);
    }

    public NeuronsFactoryImpl(SignalsFactory signalsFactory, LinkFactory linkFactory,
                    Duration sendingDelay, ReferencesFactory referencesFactory,
                    NoiseThresholdCalculator noiseThresholdCalculator,
                    StatisticCheckerFactory statisticCheckerFactory) {
        this.signalsFactory = signalsFactory;
        this.linkFactory = linkFactory;
        this.sendingDelay = sendingDelay;
        this.referencesFactory = referencesFactory;
        this.noiseThresholdCalculator = noiseThresholdCalculator;
        this.statisticCheckerFactory = statisticCheckerFactory;
    }

    @Override
    public <D> Neuron createNeuron(Predicate<DataSignal<D>> predicate, int lLimit, int gLimit) {
        return new NeuronI<>(referencesFactory.create(NeuronI.class), predicate, sendingDelay,
                        noiseThresholdCalculator, lLimit, gLimit, signalsFactory, linkFactory,
                        statisticCheckerFactory);
    }
}
