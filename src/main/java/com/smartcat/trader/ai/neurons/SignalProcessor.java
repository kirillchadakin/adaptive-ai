/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link SignalProcessor} process specific signal type.
 *
 * @param <S> type of the signal that will be processed.
 */
public interface SignalProcessor<S extends Signal> {
    /**
     * Process specified signal instance.
     *
     * @param signal that is going to be processed.
     */
    void process(@Nonnull S signal);
}
