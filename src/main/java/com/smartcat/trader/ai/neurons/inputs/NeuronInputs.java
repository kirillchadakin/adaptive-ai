/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.util.Collection;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.signals.DataSignal;

/**
 * {@link NeuronInputs} provides a common way to interact with all available neuron inputs.
 *
 * @param <S> type of the {@link DataSignal} that can be passed to the input.
 * @param <V> type of the data providing by received signals.
 */
public interface NeuronInputs<V, S extends DataSignal<V>> {
    int size();

    /**
     * Adds {@link com.smartcat.trader.ai.signals.Signal} to the collection that is associated with target {@link NeuronInput}
     * of {@link com.smartcat.trader.ai.signals.Signal}, clears collections of outdated signals and retrieves last
     * {@link com.smartcat.trader.ai.signals.Signal} from all {@link NeuronInput}s.
     *
     * @param signal adding signal
     * @return latest active signals
     */
    Collection<S> getLatestActiveSignals(S signal);

    /**
     * If {@link NeuronInput} is absent creates collection and associate it with {@link NeuronInput}.
     *
     * @param input adding input
     * @return true if input added successfully
     */
    boolean add(@Nonnull NeuronInput input);
}
