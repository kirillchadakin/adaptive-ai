/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

/**
 * NoiseThresholdCalculator.
 */
public interface NoiseThresholdCalculator {
    double calculate(double current, long m);

    double getMax();
}
