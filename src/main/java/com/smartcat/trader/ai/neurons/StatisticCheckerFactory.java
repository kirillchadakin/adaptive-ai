/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.function.Function;

import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link StatisticCheckerFactory}.
 */
public interface StatisticCheckerFactory {

    <S extends Signal> StatisticChecker<S> createStatisticChecker(long limit, String name,
                    Function<Long, S> signalCreator);
}
