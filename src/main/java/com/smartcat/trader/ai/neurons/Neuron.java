/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.time.Duration;
import java.util.Collection;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.LinkFactoryImpl.Consumer;
import com.smartcat.trader.ai.LinkFactoryImpl.Producer;
import com.smartcat.trader.ai.Referenceable;

/**
 * Neuron.
 */
public interface Neuron extends Referenceable {
    void connect(@Nonnull Consumer consumer, Duration delay, @Nonnull SignalType out);

    void connect(@Nonnull Producer producer, Duration delay, @Nonnull SignalType in);

    @Nonnull
    Consumer getConsumer(@Nonnull SignalType in);

    Collection<Consumer> getConsumers();

    Collection<Producer> getProducers();
}
