/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * {@link ReferenceImpl}.
 */
public class ReferenceImpl implements Reference {
    private final Class<?> type;
    private final long id;

    public ReferenceImpl(@Nonnull Class<?> type, long id) {
        this.type = type;
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Reference)) {
            return false;
        }
        final ReferenceImpl that = (ReferenceImpl)o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("%s [type=%s, id=%s]", getClass().getSimpleName(),
                        this.type.getSimpleName(), this.id);
    }
}
