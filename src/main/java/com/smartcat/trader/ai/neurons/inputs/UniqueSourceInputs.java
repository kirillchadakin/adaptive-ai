/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons.inputs;

import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.Signal;

/**
 * UniqueSourceInputs.
 *
 * @param <S> type of the {@link DataSignal} that can be passed to the input.
 * @param <V> type of the data providing by received signals.
 */
public class UniqueSourceInputs<V, S extends DataSignal<V>> extends AbstractInputs<V, S> {
    private final Map<NeuronInput, Deque<S>> signals;

    protected UniqueSourceInputs(
                    BiConsumer<NeuronInput, Collection<? extends Signal>> outdatedSignalsCleaner,
                    Map<NeuronInput, Deque<S>> signals, Reference neuron) {
        super(neuron, outdatedSignalsCleaner);
        this.signals = signals;
    }

    public UniqueSourceInputs(Reference neuron) {
        this(new OutdatedSignalsCleaner(), new HashMap<>(), neuron);
    }

    @Override
    public int size() {
        return signals.size();
    }

    @Override
    public boolean add(@Nonnull NeuronInput input) {
        final boolean result = signals.putIfAbsent(input, new LinkedList<>()) == null;
        if (result) {
            logNewSource(input);
        }
        return result;
    }

    @Nonnull
    @Override
    // TODO: 21.08.2022 Разделить метод на 2: добавление сигнала в очередь и сбор последних сигналов из очередей
    protected Collection<S> collectLatestActiveSignals(@Nonnull S signal) {
        final NeuronInput targetInput = signal.getTargetInput();
        final Deque<S> signalsFromSource = signals.get(targetInput);
        if (signalsFromSource == null) {
            return Collections.emptySet();
        }
        signalsFromSource.offer(signal);
        signals.forEach(this::cleanSignals);

        return signals.values().stream().map(Deque::peekLast).filter(Objects::nonNull)
                        .collect(Collectors.toSet());
    }

}
