/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link StatisticChecker} checks statistic rule and creates signal in case rule passed
 * successfully.
 *
 * @param <S> type of the {@link Signal} that could be checked by this checker.
 */
public class StatisticChecker<S extends Signal> {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String NEURON_PASSED_STATISTIC_RULE =
                    "{} passed statistic rule '{}' with '{}'";
    private final long limit;
    private final AtomicLong currentValue = new AtomicLong();
    private final AtomicBoolean currentState = new AtomicBoolean();
    private final String name;
    private final Function<Long, S> signalCreator;

    /**
     * Creates {@link StatisticChecker} instance.
     *
     * @param limit amount of signals that should be handled by this checker before
     *                 the rule will be treated as passed.
     * @param name name of the statistic checker
     * @param signalCreator creates signal when the statistic border passed
     */
    public StatisticChecker(long limit, @Nonnull String name,
                    @Nonnull Function<Long, S> signalCreator) {
        this.limit = limit;
        this.name = name;
        this.signalCreator = signalCreator;
    }

    /**
     * Creates {@link Signal} instance depending on the current state and currently received
     * signal.
     *
     * @param signal recently received signal
     * @param neuron for which received signal
     * @return new data signal in case rule passed recently or in case it passed because of
     *                 the recently received signal, otherwise returns {@code null}.
     */
    @Nullable
    public S createSignal(@Nonnull Signal signal, @Nonnull Reference neuron) {
        currentValue.incrementAndGet();
        if (currentState.get()) {
            return signalCreator.apply(currentValue.get());
        }
        final boolean newState = currentValue.get() > limit;
        final boolean result = !currentState.get() && newState;
        if (result) {
            LOGGER.info(NEURON_PASSED_STATISTIC_RULE, neuron, name, signal);
        }
        currentState.set(newState);
        if (currentState.get()) {
            return signalCreator.apply(currentValue.get());
        }
        return null;
    }

    @Override
    public String toString() {
        return String.format("%s [name=%s, limit=%s, currentValue=%s]", getClass().getSimpleName(),
                        this.name, this.limit, this.currentValue);
    }
}
