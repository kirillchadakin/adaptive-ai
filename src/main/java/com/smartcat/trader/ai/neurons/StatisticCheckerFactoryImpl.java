/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai.neurons;

import java.util.function.Function;

import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link StatisticCheckerFactoryImpl}.
 */
public class StatisticCheckerFactoryImpl implements StatisticCheckerFactory {
    @Override
    public <S extends Signal> StatisticChecker<S> createStatisticChecker(long limit, String name,
                    Function<Long, S> signalCreator) {
        return new StatisticChecker<>(limit, name, signalCreator);
    }
}
