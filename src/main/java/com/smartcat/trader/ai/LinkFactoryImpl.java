/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.smartcat.trader.ai.neurons.DataSignalProcessor;
import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.neurons.SignalProcessor;
import com.smartcat.trader.ai.neurons.inputs.NeuronInputImpl;
import com.smartcat.trader.ai.signals.DataSignal;
import com.smartcat.trader.ai.signals.DisablingSignal;
import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link LinkFactoryImpl} creates {@link Producer}, {@link Consumer} instances and link between
 * those instances.
 */
public class LinkFactoryImpl implements LinkFactory {
    private final Duration defaultSendDelay;
    private final ExecutorService executorService;
    private final AtomicLong linkCounter = new AtomicLong(1);

    /**
     * Creates {@link LinkFactoryImpl} instance.
     *
     * @param defaultSendDelay default delay that will be used for sending signals.
     * @param executorService thread pool in which consumers and producers will be
     *                 implicitly scheduled.
     */
    public LinkFactoryImpl(@Nonnull Duration defaultSendDelay,
                    @Nonnull ExecutorService executorService) {
        this.defaultSendDelay = defaultSendDelay;
        this.executorService = executorService;
    }

    @Nonnull
    @Override
    public <P> ProducerBuilder<P> producer(@Nonnull SignalSupplier<?> signalSupplier) {
        return new ProducerBuilder<>(signalSupplier);
    }

    @Nonnull
    @Override
    public <P> ConsumerBuilder<P> consumer(@Nonnull Class<? extends Signal> signalType,
                    @Nonnull SignalProcessor<?> signalProcessor) {
        return new ConsumerBuilder<>(signalType, signalProcessor);
    }

    /**
     * {@link AbstractLinkBuilder} contains common description of the {@link Connectable} object.
     *
     * @param <P> type of the enclosing instance for the {@link Connectable}
     *                 instance.
     * @param <R> type of the {@link Connectable} instance to which result {@link
     *                 Connectable} object will be able to connect to.
     * @param <B> recursive link to the builder type itself.
     */
    private abstract class AbstractLinkBuilder<P, R extends AbstractLink<R, ?>, B extends AbstractLinkBuilder<P, R, B>> {
        private final long identifier;
        private final Class<R> type;
        private P parent;

        /**
         * Creates {@link AbstractLinkBuilder} instance.
         *
         * @param type type which instance will be created as a result.
         */
        protected AbstractLinkBuilder(@Nonnull Class<R> type) {
            this.type = type;
            this.identifier = linkCounter.incrementAndGet();
        }

        @Nonnull
        public B setParent(@Nonnull P parent) {
            this.parent = parent;
            return getThis();
        }

        /**
         * Returns reference to the current builder instance.
         *
         * @return reference to the current builder instance.
         */
        @Nonnull
        protected abstract B getThis();

        /**
         * Populate properties specific for the descendant types.
         *
         * @param identifier of the {@link Connectable} object that will be
         *                 created.
         * @return instance of the {@link Connectable} object.
         */
        @Nonnull
        protected abstract R buildInternally(@Nonnull Reference identifier);

        @Nonnull
        public R build() {
            final R result = buildInternally(new LinkReferenceImpl<>(parent, type, identifier));
            executorService.submit(result);
            return result;
        }
    }


    /**
     * {@link ConsumerBuilder} builds {@link Consumer} instances.
     *
     * @param <P> type of the enclosing instance for the {@link Connectable}
     *                 instance.
     */
    public class ConsumerBuilder<P> extends AbstractLinkBuilder<P, Consumer, ConsumerBuilder<P>> {
        private final ImmutableMap.Builder<Class<? extends Signal>, SignalProcessor<?>>
                        signalTypeToProcessorBuilder = ImmutableMap.builder();

        private ConsumerBuilder(Class<? extends Signal> signalType,
                        SignalProcessor<?> signalProcessor) {
            super(Consumer.class);
            signalTypeToProcessorBuilder.put(signalType, signalProcessor);
        }

        /**
         * Sets implementation of the {@link SignalProcessor} that will handle {@link DataSignal}s.
         *
         * @param processor that will handle {@link DataSignal}s.
         * @param <V> type of the data contained in processing {@link DataSignal}s.
         * @return self
         */
        public <V> ConsumerBuilder<P> setDataSignalProcessor(
                        SignalProcessor<? extends DataSignal<V>> processor) {
            signalTypeToProcessorBuilder.put(DataSignal.class, processor);
            return this;
        }

        /**
         * Sets implementation of the {@link SignalProcessor} that will handle {@link
         * DisablingSignal}s.
         *
         * @param processor that will handle {@link DisablingSignal}s.
         * @return self
         */
        public ConsumerBuilder<P> setDisablingSignalProcessor(
                        SignalProcessor<? extends DisablingSignal> processor) {
            signalTypeToProcessorBuilder.put(DisablingSignal.class, processor);
            return this;
        }

        /**
         * Sets mapping from {@link Signal} type to implementation of the {@link SignalProcessor}
         * that will handle appropriate {@link Signal}s.
         *
         * @param signalTypeToProcessor mapping from {@link Signal} to appropriate
         *                 {@link SignalProcessor}
         * @return self
         */
        public ConsumerBuilder<P> setSignalTypeToProcessor(
                        Map<Class<? extends Signal>, SignalProcessor<?>> signalTypeToProcessor) {
            signalTypeToProcessorBuilder.putAll(signalTypeToProcessor);
            return this;
        }

        @Nonnull
        @Override
        protected ConsumerBuilder<P> getThis() {
            return this;
        }

        @Nonnull
        @Override
        protected Consumer buildInternally(@Nonnull Reference identifier) {
            return new Consumer(identifier, signalTypeToProcessorBuilder.build());
        }
    }


    /**
     * {@link ProducerBuilder} builds {@link Producer} instances.
     *
     * @param <P> type of the enclosing instance for the {@link Connectable}
     *                 instance.
     */
    public class ProducerBuilder<P> extends AbstractLinkBuilder<P, Producer, ProducerBuilder<P>> {
        private final SignalSupplier<?> signalSupplier;
        private Duration sendDelay;

        private ProducerBuilder(@Nonnull SignalSupplier<?> signalSupplier) {
            super(Producer.class);
            this.signalSupplier = signalSupplier;
        }

        @Nonnull
        @Override
        protected ProducerBuilder<P> getThis() {
            return this;
        }

        @Nonnull
        @Override
        protected Producer buildInternally(@Nonnull Reference identifier) {
            return new Producer(identifier, signalSupplier, getSendDelay());
        }

        /**
         * Sets delay before sending next signal by {@link Producer} instance.
         *
         * @param sendDelay desired delay.
         * @return self
         */
        @Nonnull
        public ProducerBuilder<P> setSendDelay(@Nonnull Duration sendDelay) {
            this.sendDelay = sendDelay;
            return this;
        }

        private Duration getSendDelay() {
            return sendDelay == null ? defaultSendDelay : sendDelay;
        }
    }


    /**
     * {@link Producer} creates {@link Signal} instances and send them to all connected {@link
     * Consumer} instances.
     */
    public static class Producer extends AbstractLink<Producer, Consumer> {
        private static final Logger LOGGER = LogManager.getLogger();
        private final SignalSupplier<? extends Signal> signalSupplier;
        private final Duration sendingDelay;

        private Producer(Reference identifier, SignalSupplier<? extends Signal> signalSupplier,
                        Duration sendingDelay) {
            super(identifier);
            this.signalSupplier = signalSupplier;
            this.sendingDelay = sendingDelay;
        }

        @Override
        public Void call() throws InterruptedException {
            while (true) {
                final Signal signal = signalSupplier.get();
                LOGGER.trace("Signal '{}' supplied by '{}'", signal,
                                signalSupplier.getClass().getSimpleName());
                notifyLinks(l -> l.signals.add(signal));
                Thread.sleep(sendingDelay.toMillis());
            }
        }
    }


    /**
     * {@link SignalSupplier} provides {@link Signal} instances, while getting {@link Signal}
     * instance it might throw {@link InterruptedException}.
     *
     * @param <S> type of the {@link Signal} that will be provided.
     */
    public interface SignalSupplier<S extends Signal> {
        /**
         * Provides {@link Signal} instance.
         *
         * @return {@link Signal} instance.
         * @throws InterruptedException in case getting of the next {@link Signal}
         *                 has been interrupted.
         */
        S get() throws InterruptedException;
    }


    /**
     * {@link Consumer} retrieves and process {@link Signal}s sent by {@link Producer}s connected to
     * the current consumer.
     */
    public static class Consumer extends AbstractLink<Consumer, Producer> {
        private static final Logger LOGGER = LogManager.getLogger();
        private final BlockingQueue<Signal> signals = new LinkedBlockingQueue<>();
        private final Map<Class<? extends Signal>, SignalProcessor<?>> signalTypeToProcessor;

        private Consumer(Reference identifier,
                        Map<Class<? extends Signal>, SignalProcessor<?>> signalTypeToProcessor) {
            super(identifier);
            this.signalTypeToProcessor = signalTypeToProcessor;
        }

        @Override
        public Void call() throws InterruptedException {
            while (true) {
                final Signal latestSignal = signals.take();
                LOGGER.trace("Signal '{}' received in '{}' consumer", latestSignal, getRef());
                processLatestSignal(latestSignal);
            }
        }

        @Override
        public void connect(Producer object, Duration delay) {
            super.connect(object, delay);
            signalTypeToProcessor.values().stream().filter(DataSignalProcessor.class::isInstance)
                            .map(DataSignalProcessor.class::cast).forEach(processor -> processor
                            .register(new NeuronInputImpl(object.getRef(), delay)));
        }

        private <S extends Signal> void processLatestSignal(S latestSignal) {
            @SuppressWarnings("unchecked")
            final SignalProcessor<S> signalProcessor =
                            (SignalProcessor<S>)signalTypeToProcessor.get(latestSignal.getClass());
            if (signalProcessor == null) {
                return;
            }
            signalProcessor.process(latestSignal);
        }
    }


    /**
     * {@link AbstractLink} contains common properties and functionality for {@link Connectable}
     * interface.
     *
     * @param <V> type of the links with which current object will be able to
     *                 interact.
     * @param <T> type of the links with which current object will be able to
     *                 interact.
     */
    private abstract static class AbstractLink<V extends AbstractLink<V, T>, T extends Connectable<V>>
                    implements Connectable<T>, Referenceable {
        private final Collection<T> links = new CopyOnWriteArraySet<>();
        private final Reference ref;

        protected AbstractLink(@Nonnull Reference ref) {
            this.ref = ref;
        }

        @Override
        public void connect(T object, Duration delay) {
            if (links.add(object)) {
                @SuppressWarnings("unchecked")
                final V typed = (V)this;
                object.connect(typed, delay);
            }
        }

        protected void notifyLinks(java.util.function.Consumer<T> notifier) {
            for (T link : links) {
                notifier.accept(link);
            }
        }

        @Nonnull
        public Reference getRef() {
            return ref;
        }
    }


    /**
     * {@link Connectable} interface which should be implemented by all classes that are able to
     * connect to some different objects of the specific type.
     *
     * @param <T> type of the object to which it is able to connect to.
     */
    public interface Connectable<T> extends Callable<Void> {
        /**
         * Connects current object and specified object of the desired type.
         *
         * @param object object to which it will be connected to.
         * @param delay of the received signal handling
         */
        void connect(T object, Duration delay);
    }


    /**
     * {@link LinkReferenceImpl} reference to the link instance.
     *
     * @param <P> type of the enclosing instance for the {@link Connectable}
     *                 instance.
     */
    private static class LinkReferenceImpl<P> implements Reference {
        private final P parent;
        private final Class<? extends Connectable<?>> type;
        private final long identifier;

        private LinkReferenceImpl(@Nullable P parent, @Nonnull Class<? extends Connectable<?>> type,
                        long identifier) {
            this.parent = parent;
            this.type = type;
            this.identifier = identifier;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof LinkReferenceImpl)) {
                return false;
            }
            final LinkReferenceImpl<?> that = (LinkReferenceImpl<?>)o;
            return identifier == that.identifier
                            && Objects.equals(parent, that.parent)
                            && type == that.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(parent, type, identifier);
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            if (parent != null) {
                builder.append(parent).append(":");
            }
            builder.append(type.getSimpleName()).append("#").append(identifier);
            return builder.toString();
        }

        @Override
        public long getId() {
            return identifier;
        }
    }

}
