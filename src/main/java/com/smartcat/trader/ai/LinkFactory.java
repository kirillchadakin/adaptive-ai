/*
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.LinkFactoryImpl.Consumer;
import com.smartcat.trader.ai.LinkFactoryImpl.ConsumerBuilder;
import com.smartcat.trader.ai.LinkFactoryImpl.Producer;
import com.smartcat.trader.ai.LinkFactoryImpl.ProducerBuilder;
import com.smartcat.trader.ai.LinkFactoryImpl.SignalSupplier;
import com.smartcat.trader.ai.neurons.SignalProcessor;
import com.smartcat.trader.ai.signals.Signal;

/**
 * {@link LinkFactory} creates {@link ProducerBuilder}, {@link ConsumerBuilder} to build {@link
 * Producer} and {@link Consumer} instances.
 */
public interface LinkFactory {

    /**
     * Creates {@link ProducerBuilder} instance.
     *
     * @param signalSupplier supplier of the signals that will be send by {@link
     *                 Producer} instance.
     * @param <P> type of the enclosing instance for the {@link Producer}.
     * @return {@link ProducerBuilder} instance.
     */
    @Nonnull
    <P> ProducerBuilder<P> producer(@Nonnull SignalSupplier<?> signalSupplier);

    /**
     * Creates {@link ConsumerBuilder} instance.
     *
     * @param signalType type of the signals that could be consumed by {@link
     *                 Consumer} instance.
     * @param signalProcessor instance of the processor that will handle signals
     *                 consumed by {@link Consumer}.
     * @param <P> type of the enclosing instance for the {@link Consumer}.
     * @return {@link ProducerBuilder} instance.
     */
    @Nonnull
    <P> ConsumerBuilder<P> consumer(@Nonnull Class<? extends Signal> signalType,
                    @Nonnull SignalProcessor<?> signalProcessor);

}
