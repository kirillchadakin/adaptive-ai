/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai;

import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;
import com.smartcat.trader.ai.neurons.ReferenceImpl;

/**
 * {@link ReferencesFactory} generates unique references for {@link Referenceable} instances across
 * all the system.
 */
public class ReferencesFactory {
    private final AtomicLong identifierGenerator = new AtomicLong();

    @Nonnull
    public <T> Reference create(@Nonnull Class<T> type) {
        return new ReferenceImpl(type, identifierGenerator.getAndIncrement());
    }

}
