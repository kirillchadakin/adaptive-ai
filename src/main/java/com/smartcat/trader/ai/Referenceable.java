/**
 * (C) Turbonomic 2019.
 */

package com.smartcat.trader.ai;

import javax.annotation.Nonnull;

import com.smartcat.trader.ai.neurons.Reference;

/**
 * Referenceable.
 */
public interface Referenceable {

    @Nonnull
    Reference getRef();
}
